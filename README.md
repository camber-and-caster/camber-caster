# CAMBER & CASTER #



### What is CAMBER & CASTER? ###

**CAMBER** (**C**ar **A**utomation **M**anaged **B**y **E**V **R**emote) & 
**CASTER** (**C**ar **A**nimated **S**imulation **T**echnology for **E**V **R**emote) are web apps
that use **FordConnect** APIs to deliver remote commands to actual and simulated **Ford** vehicles.